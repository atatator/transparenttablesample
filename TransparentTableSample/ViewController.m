//
//  ViewController.m
//  TransparentTableSample
//
//  Created by Peter Prokop on 7/10/13.
//  Copyright (c) 2013 Prokop. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
{
    NSMutableArray* _data;
    NSInteger _rowToDelete;

    __weak IBOutlet UITableView *_tableView;
    IBOutlet UITableViewCell *_cell;
}

@end

typedef enum {
    TAG_CELL_LABEL = 1001
} CellTag;

@implementation ViewController



- (void)viewDidLoad
{
    [super viewDidLoad];

    _rowToDelete = -1;
    
    _data = [NSMutableArray arrayWithObjects:@"Harvard University",
             @"Stanford University",
             @"Moscow State University",
             @"MIT",
             @"Unseen University",
             @"Hogwarts",
             @"Harvard University",
             @"Stanford University",
             @"Moscow State University",
             @"MIT",
             @"Unseen University",
             @"Hogwarts",
             nil];
}

- (void)viewDidUnload
{
    _cell = nil;
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_data count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == _rowToDelete)
        return 0;
    
    return 44;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cell;
    NSString* identifier = @"my cell";
    cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell == nil)
    {
        [[NSBundle mainBundle] loadNibNamed:@"MyCell" owner:self options:nil];
        cell = _cell;
        _cell = nil;
    }
    
    UILabel* label = (UILabel*)[cell viewWithTag:TAG_CELL_LABEL];
    label.text = [_data objectAtIndex:indexPath.row];
    
    return cell;
}

#pragma mark - UI

// The not so obvious way
- (IBAction)collapseRow:(id)sender
{
    NSIndexPath* indexPath = [self getCellIndexPathForSubview:(UIView *)sender];
    if(!indexPath)
        return;
    
    [_tableView beginUpdates];
    _rowToDelete = indexPath.row;
    [_tableView endUpdates];
    [self performSelector:@selector(deleteData) withObject:nil afterDelay:0.3f];
}

// The easy way
- (IBAction)fadeOutRow:(id)sender
{
    NSIndexPath* indexPath = [self getCellIndexPathForSubview:(UIView *)sender];
    if(!indexPath)
        return;
    
    [_tableView beginUpdates];
    [_data removeObjectAtIndex:indexPath.row];
    [_tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                      withRowAnimation:(UITableViewRowAnimationFade)];
    [_tableView endUpdates];
}

#pragma mark - Other

- (NSIndexPath*)getCellIndexPathForSubview:(UIView*)view
{
    UIView* superview = view.superview;
    
    while (![superview isKindOfClass:[UITableViewCell class]])
    {
        superview = superview.superview;
        if(superview == nil)
            return nil;
    }
    
    return [_tableView indexPathForCell:(UITableViewCell*)superview];
}

- (void)deleteData
{
    [_data removeObjectAtIndex:_rowToDelete];
    _rowToDelete = -1;
    [_tableView reloadData];
}

@end