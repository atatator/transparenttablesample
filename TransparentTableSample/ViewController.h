//
//  ViewController.h
//  TransparentTableSample
//
//  Created by Peter Prokop on 7/10/13.
//  Copyright (c) 2013 Prokop. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>

@end
