//
//  AppDelegate.h
//  TransparentTableSample
//
//  Created by Peter Prokop on 7/10/13.
//  Copyright (c) 2013 Prokop. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@end
